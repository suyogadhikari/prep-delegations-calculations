global cursor
import urllib.parse
import json
import requests
import _csv as csv
from pymongo import MongoClient
#connecting with server database
username = urllib.parse.quote_plus('ajaya')
password = urllib.parse.quote_plus('mypassword')
client = MongoClient('mongodb://%s:%s@54.186.60.134/transaction' % (username, password))
db = client["transaction"]
collection = db["information"]



address =[]
prep=[]
votes2=[]
list_dict =[]


# finding out total number of prep present before decentralization
# cursor1 = db.information.distinct('prep_address', {'date': {'$lt':'2019-10-30T00:00:00.000+0000' }})
k = 0
# for x in cursor1:
#     print(x)
#     prep.append(x)
# print(prep)
# print(len(prep))

sum2=[]
fromAddress2 =[]
prep = []
failed=[]
unregistered=[]
res = requests.get(
    'https://tracker.icon.foundation/v3/iiss/prep/list?count=500')

data = res.json()
data = data['data']

for x in data:
    prep.append(x['address'])
print(prep)
for y in prep:
    k+=1
    print('-------------------------------------------------'+str(k)+'-------------------------------------------------------------------')
    dict1 = {}
    # finding out all the transaction of the specific prep before given date sorted by date in an descending order
    try:
        cursor =db.information.find({'$and': [{'prep_address':y},{'date': {'$lt':'2019-10-30T00:00:00.000+0000' }}]},no_cursor_timeout=True).sort([{'date',-1}]).batch_size(10)
        if cursor.count()!=0:
            fromAddress = []
            prep = []
            votes = []
            for x in cursor:
                print('start')
                # if fromaddress is already in the list that means its latest transactions are already recorded by the script
                if x['fromAddress'] in fromAddress:
                    print('not done')
                else:
                    #if from address is not  in list then certain calculations of delegations are done
                    print('else')
                    fromAddress.append(x['fromAddress'])
                    # finding out latest transaction of that from address before given date and finding out its transaction hash
                    cursor2 = db.information.find({'$and': [{'fromAddress':x['fromAddress']},{'date': {'$lt':'2019-10-30T00:00:00.000+0000' }}]},no_cursor_timeout=True).sort([{'date',-1}]).limit(1).batch_size(10)
                    for p in cursor2:
                        txHash = p['txHash']
                        # finding out the transaction hash
                        cursor3 = db.information.find({'txHash':txHash},no_cursor_timeout=True)
                        for q in cursor3:
                            #checking if the transaction hash has transactions with this prep address
                            if(q['prep_address'] == x['prep_address']):
                                if x['value'] == '0.0':
                                    break
                                else:
                                    fromAddress2.append(x['fromAddress'])
                                    votes.append(float(x['value']))


            sum1 = sum(votes)
            # finding sum of a particular prep and appending that on list
            sum2.append(sum1)
            address.append(y)
            #creating a dictionary of prep_address and their votes and appending it in list_dict
            dict1 = {y:sum1}
            list_dict.append(dict1)

        else:
            print(y)
            print('unregistered prep')
            unregistered.append(y)

    except Exception as e:
        print(e)
        failed.append(y)

print(unregistered)
delegations1=[]
print(list_dict)
print(sum2)
voted = sum(sum2)
print('failed')
print(failed)

try:
    rows2 = zip(sum2,address,unregistered)
    import _csv as csv
    with open('votes_prep.csv','w',newline='') as f:
        writer1 =  csv.writer(f, delimiter=' ', quoting=csv.QUOTE_MINIMAL)
        writer1.writerows(rows2)



    f.close()
except:
    print('csv not supported votes')

try:
    rows2 = zip(sum2, address, unregistered)
    import csv
    with open('votes_prep1.csv','w',newline='') as f:
        writer1 =  csv.writer(f, delimiter=' ', quoting=csv.QUOTE_MINIMAL)
        writer1.writerows(rows2)


    f.close()
except:
    print('csv not supported votes')


# to find out percentage of delegations of votes of prep
for x in sum2:
    delegations = (x/voted)*100
    delegations1.append(delegations)


# to find out the name of prep and if there are some prep that were present before decentralization but not now then "Not Known" name is given to them
res = requests.get(
    'https://tracker.icon.foundation/v3/iiss/prep/list?count=500')

data = res.json()
data = data['data']

data1={}
name3 =[]
for x in data:
    name = x['name']
    add = x['address']
    data1[add] = name
# created a dictionary data1 that has recent prep_address as key and its name as a value
print(data1)

for x in address:
    #if prep_address present before decentralization is present till now then name is given from dictionary data1
    if x in data1:
        name2 = data1[x]
        name3.append(name2)
    else:
        #if prep address present before decentralization is not present currently then 'Not known' name is given
        name2 = 'Not Known'
        name3.append(name2)

print(name3)
#name3 consists of the name of prep address
print(delegations1)
# delegations1 consists of the percentage of their delegations
print(address)
# address consists of the prep_address


columns = {}
columns['name']      = name3
columns['delegations'] = delegations1
columns['address'] = address

rows = zip(columns['name'],columns['delegations'],columns['address'])


try:
    import _csv as csv
    with open('delegations_prep.csv','w',newline='') as f:
        writer1 =  csv.writer(f, delimiter=' ', quoting=csv.QUOTE_MINIMAL)
        writer1.writerows(rows)

    f.close()
except:
    print('csv not supported')

try:
    import csv
    with open('delegations_prep2.csv','w',newline='') as f:
        writer1 =  csv.writer(f, delimiter=' ', quoting=csv.QUOTE_MINIMAL)
        writer1.writerows(rows)

    f.close()
except:
    print('csv not supported')
